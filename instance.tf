# AWS Config


# Configure the AWS Provider


provider "aws" {
  version = "~> 2.0"
   region  = "us-east-1"
    }



#Configure Resource


resource "aws_instance" "softtek-example001" {
   ami		= "ami-035b3c7efe6d061d5"
  instance_type = "t2.micro"
  subnet_id = "subnet-03a0d564"
 

tags = {
    Name = "Softtek-Instance"
    OS 	 = "Linux 64 bit"
    Random = "Additional text"

      }

  }
