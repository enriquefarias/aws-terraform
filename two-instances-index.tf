# AWS Config


# Configure the AWS Provider


provider "aws" {
  version = "~> 2.0"
   region  = "us-east-1"
   shared_credentials_file ="~/.aws/credentials"
    }


#Variables

variable "instance_count" {
  default = "2"
  }

  variable "instance_tags" {
    type = "list"
      default = ["Terraform-1", "Terraform-2"]
      }


#Configure Resource


resource "aws_instance" "softtek" {
 count         = "${var.instance_count}"
   ami          = "ami-035b3c7efe6d061d5"
  instance_type = "t2.micro"
  subnet_id = "subnet-03a0d564"



 tags = {
     Name  = "${element(var.instance_tags, count.index)}"
     OS          = "Linux 64 bit"
     Random = "Additional text"

      }

  }

